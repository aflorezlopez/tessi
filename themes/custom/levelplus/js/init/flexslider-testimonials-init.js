jQuery(document).ready(function($) {
  if ($(".view-testimonials-slider").length>0){
    $(window).load(function() {
      $(".view-testimonials-slider .flexslider").fadeIn("slow");
      $(".view-testimonials-slider .flexslider").flexslider({
        animation: drupalSettings.levelplus.flexsliderTestimonialsInit.TestimonialsSliderEffect,
        slideshowSpeed: drupalSettings.levelplus.flexsliderTestimonialsInit.TestimonialsSliderEffectTime,
        useCSS: false,
        prevText: "prev",
        nextText: "next",
        controlNav: false
      });
    });
  }
});
