jQuery(document).ready(function($) {
  $("body").addClass("video-bg-active");
  $(".video-bg-active #highlighted-bottom").vide({
    mp4: drupalSettings.levelplus.VideoBackgroundInit.PathToVideo_mp4,
    webm: drupalSettings.levelplus.VideoBackgroundInit.PathToVideo_webm,
    poster: drupalSettings.levelplus.VideoBackgroundInit.pathToVideo_jpg
  },{
    posterType: 'jpg',
    className: 'video-container'
  });
});