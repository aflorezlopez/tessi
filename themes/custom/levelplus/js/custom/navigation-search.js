jQuery(document).ready(function($) {
  $( "#search-area #block-mt-navigation-search .form-search" ).focusin(function() {
    $("#search-area").addClass("opened");
  });
  $( "#search-area #block-mt-navigation-search .form-search" ).focusout(function() {
    $("#search-area").removeClass("opened");
  });
});
